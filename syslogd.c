#define _DEFAULT_SOURCE
#define _XOPEN_SOURCE 500     /* or any value > 500 */
#include <stdio.h>
#include <mysql/mysql.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <resolv.h>
#include <arpa/inet.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/time.h>
#include <memory.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <stdarg.h>
#include <sys/termios.h>
#include <math.h>
#include <sys/un.h>
/*Global variables*/
#define PORT 514
#define SA struct sockaddr
char *buffer = "";
char *path1 = "/dev/log";
/*
Juan Daniel Cortez Rojas
danielitos0901@ciencias.unam.mx
No. cuenta: 313142053
*/

static int create_socket(char* path){
  /*Structure for the config of the socket*/
    struct sockaddr_un sunx;
  /*socket id*/
    int sock_fd;
  /*place of the memory addres*/
    char *dev_log_name;
  /*asigning the space for the socket configuration struct*/
    memset(&sunx, 0, sizeof(sunx));
  /*asigning the type of socket*/
    sunx.sun_family = AF_UNIX;

    /* Unlink old /dev/log or object it points to. */
    /* (if it exists, bind will fail) */
    strcpy(sunx.sun_path, path);
  /*this line get the absoluthe path in the sistem*/
    dev_log_name = realpath(path, NULL);
  /*if there is no problem we continue*/
  if (dev_log_name) {
    /*coping the path into the config struct*/
    printf("Path: %s\n", dev_log_name);
    strncpy(sunx.sun_path, dev_log_name, sizeof(sunx.sun_path));
    /*cleaning the memory*/
    free(dev_log_name);
  }
  /*if we had a problem while setting the path */
  else{
    printf("Error while assigning the path: %s\n", path);
    return -1;
  }

  unlink(sunx.sun_path);
  
  /*creating the socket with the specific path*/
    sock_fd = socket(AF_UNIX, SOCK_DGRAM, 0);
  if(sock_fd < 0){
    printf("Error while creating socket\n");
    return -1;
  } else {
    printf("Socket Id: %d\n",sock_fd);
  }
  /*assigning the config struct*/
    if((bind(sock_fd, (struct sockaddr *) &sunx, sizeof(sunx))) != 0) {
      printf("socket bind failed...\n");
      //exit(0);
  } else {
    printf("socket binded correctly\n");
  }
  
  if (connect(sock_fd, (struct sockaddr *)&sunx, sizeof(sunx))    < 0){
    printf("\nConnection Failed \n");
    return -1;  
  }
  printf("conectado correctamente\n");
  //giving permissions to the user for writing and reading the directorie.
    chmod(path, 0666);
  /*returning socket's id*/
    return sock_fd;
}

static int create_tcp_socket(int port){
  struct sockaddr_in servaddr;

  int addrlen = sizeof(servaddr); 
  int new_socket;
  /*Creating and vering the socket*/
  int socket_id = socket(AF_INET , SOCK_STREAM , 0);
  /*getting current status from */
  if(socket_id < 0){
    printf("Error while creating socket\n");
    return -1;
  }
  else {
    printf("Socket id:%d\n", socket_id);
  }
  /*Cleaning the struct*/
  bzero(&servaddr, sizeof(servaddr));

  /*assign  assign IP y PORT al socket*/
  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servaddr.sin_port = htons(PORT);
  
  /*Binding newly created socket to given IP and verification*/
  if ((bind(socket_id, (SA*)&servaddr, sizeof(servaddr))) != 0) {
      printf("socket bind failed...-----\n");
  }
  else
      printf("Socket successfully binded...\n");
 
  if (listen(socket_id, 3) < 0) 
  { 
      perror("listen"); 
      exit(EXIT_FAILURE); 
  } 
  
  if ((new_socket = accept(socket_id, (struct sockaddr *)&servaddr,  
                    (socklen_t*)&addrlen))<0) 
  { 
      perror("accept"); 
      exit(EXIT_FAILURE); 
  }  
    
  return socket_id;
}

/*
 * Read info from socket and print into terminal
 * TODO: print into var\log
 */
static int read_from_socket(int socket){
  size_t recived_bytes = 0;
  int len = 0;
  memset(buffer, '\0', sizeof(buffer));
  /*reading number of bits send in socket*/
  ioctl(socket, FIONREAD, &len);
  /*
   * creating a buffer of the specified size of
   * the message in socket
  */
  buffer = (char *) malloc(len*sizeof(char));
  
  /*getting the message and storing in buffer*/
  recived_bytes = recv(socket, buffer, len, 0);
  
  if(recived_bytes == -1){
    printf("Error in recv, recived_bytes = -1\n");
    return -1;
  } else{
    printf("recived_bytes:%d\n", recived_bytes);
    printf("len:%d\n", len);
    printf("-----------------\n");
    return 1;
  }
}

int write_with_socket(int socket){
  int length = strlen(buffer);
  printf("bandera write: %s(%d)\n", buffer, length); 
  int valread = send(socket, buffer, length, 0);
  if(valread < 0){
    printf("mensaje no enviado o enviado con errores\n");
    return -1;
  }
  return 1;
}

int write_to_db(char* message){
  MYSQL *conn;

  char *server = "localhost";
  char *user = "guest";
  char *password = "Password1+"; /* set me first */
  char *database = "syslogd_db";

  conn = mysql_init(NULL);

   /* Connect to database */
  if (!mysql_real_connect(conn, server,
    user, password, database, 0, NULL, 0)) {
    fprintf(stderr, "%s\n", mysql_error(conn));
    return -1;
  }
  /* getting space for the string query*/
  int message_len = strlen(message);
  int length1 = message_len + 37;
  char query[length1];

  
  char *insert = "insert into msg (mensaje) values ('";
  char *in2 = "')";
  /*making the query*/
  printf("Bandera1\n");
  strcat(query, insert);  
  printf("Bandera2\n");  
  strcat(query, message);
  strcat(query, in2);

  int length = strlen(query);

  if (mysql_query(conn, query)){ 
    fprintf(stderr, "%s\n", mysql_error(conn));
    return -1;
  }
  
  mysql_close(conn);
  return 1;
}

int main(void) {
    /* Our process ID and Session ID */
    pid_t pid, sid;
    
    /* Fork off the parent process */
    pid = fork();
    if (pid < 0) {
            exit(EXIT_FAILURE);
    }
    /* If we got a good PID, then
       we can exit the parent process. */
    if (pid > 0) {
            exit(EXIT_SUCCESS);
    }

    /* Change the file mode mask */
    umask(0);
            
    /* Open any logs here */        
            
    /* Create a new SID for the child process */
    sid = setsid();
    if (sid < 0) {
            /* Log the failure */
            exit(EXIT_FAILURE);
    }
    

    
    /* Change the current working directory */
    if ((chdir("/")) < 0) {
            /* Log the failure */
            exit(EXIT_FAILURE);
    }
    
    /* Close out the standard file descriptors */
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    
    /* Daemon-specific initialization goes here */
    
    /* The Big Loop */
    while (1) {
       /* Do some task here ... */

        int log = create_socket("/var/log/msg");
        printf("-----------------\n");
        int kmsg = create_socket("/dev/kmsg");
        printf("-----------------\n");
        int tcp = create_tcp_socket(514);
        printf("-----------------\n");

        
        int tcp_read = read_from_socket(tcp);
        if(tcp_read < 0){
          printf("Cant read from tcp socket with id: %d\n", tcp);
          exit(-1);
        }
        
        int kmsg_write = write_with_socket(kmsg);
        if(kmsg_write < 0){
          printf("Cant write to kmsg socket with id: %d\n", kmsg);
        }
        printf("escrito correctamente a kmsg\n"); 
        int db_write = write_to_db(buffer);
        if(db_write < 0){
          printf("Cant write to data base\n");
        }
        
        int log_write = write_with_socket(log);
        if(log_write < 0){
          printf("Cant write to log  socket with id: %d\n", log);
        }
        printf("escrito correctamente a log\n");   
        sleep(30); /* wait 30 seconds */
    }
    exit(EXIT_SUCCESS);
}