CREATE DATABASE syslogd_db;
/*
GRANT ALL PRIVILEGES ON syslogd_db.* TO 'guest'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON syslogd_db.* TO 'guest'@'localhost';
*/
USE syslogd_db;
CREATE TABLE msg (
    id INT auto_increment, 
    mensaje VARCHAR(254), 
    primary key(id)
);
